const express = require('express');
const routes = require('./api/routes');
const winston = require('winston');
const expressWinston = require('express-winston');
const statEmitter = require('./api/subscribers/emitter');
const stats = require('./api/repositories/stats');
const db = require('./db/db');
const envalid = require('envalid');

const { str, num } = envalid;
const app = express();
const port = 3000;

envalid.cleanEnv(process.env, {
  DATABASE_PORT: num(),
  DATABASE_HOST: str(),
  DATABASE_NAME: str(),
  DATABASE_USER: str(),
  DATABASE_ACCESS_KEY: str(),
  JWT_SECRET: str(),
});

app.use(express.json());

app.use((req, res, next) => {
  db.raw('select 1+1 as result')
    .then(function () {
      next();
    })
    .catch((err) => {
      console.error(`No db connection (${err.message})`);
      process.exit(0);
    });
});

app.use(
  expressWinston.logger({
    transports: [new winston.transports.Console()],
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.json()
    ),
    meta: false,
    msg: 'HTTP Method: {{req.method}}, URL: {{req.url}}, Respose time: {{res.responseTime}}ms, Result status: {{res.statusCode}}',
    expressFormat: false,
    colorize: false,
    ignoreRoute: function (req, res) {
      return false;
    },
  })
);

routes(app);

app.listen(port, () => {
  statEmitter.on('newUser', () => {
    stats.totalUsers++;
  });
  statEmitter.on('newBet', () => {
    stats.totalBets++;
  });
  statEmitter.on('newEvent', () => {
    stats.totalEvents++;
  });

  console.log(`App listening at http://localhost:${port}`);
});

// Do not change this line
module.exports = { app };
