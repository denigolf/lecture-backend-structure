const stats = require('../repositories/stats');

class StatsController {
  getStats(req, res) {
    try {
      res.send(stats);
    } catch (err) {
      console.log(err);
      res.status(500).send('Internal Server Error');
      return;
    }
  }
}

module.exports = new StatsController();
