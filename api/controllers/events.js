const eventsService = require('../services/events');
const usersService = require('../services/users');
const betsService = require('../services/bets');
const oddsService = require('../services/odds');
const statEmitter = require('../subscribers/emitter');

class EventsController {
  createEvent(req, res) {
    try {
      req.body.odds.home_win = req.body.odds.homeWin;
      delete req.body.odds.homeWin;
      req.body.odds.away_win = req.body.odds.awayWin;
      delete req.body.odds.awayWin;

      oddsService.createOdd(req.body.odds).then(([odds]) => {
        delete req.body.odds;
        req.body.away_team = req.body.awayTeam;
        req.body.home_team = req.body.homeTeam;
        req.body.start_at = req.body.startAt;
        delete req.body.awayTeam;
        delete req.body.homeTeam;
        delete req.body.startAt;

        eventsService.createEvent(req.body, odds.id).then(([event]) => {
          statEmitter.emit('newEvent');
          [
            'bet_amount',
            'event_id',
            'away_team',
            'home_team',
            'odds_id',
            'start_at',
            'updated_at',
            'created_at',
          ].forEach((whatakey) => {
            let index = whatakey.indexOf('_');
            let newKey = whatakey.replace('_', '');
            newKey = newKey.split('');
            newKey[index] = newKey[index].toUpperCase();
            newKey = newKey.join('');
            event[newKey] = event[whatakey];
            delete event[whatakey];
          });
          ['home_win', 'away_win', 'created_at', 'updated_at'].forEach(
            (whatakey) => {
              let index = whatakey.indexOf('_');
              let newKey = whatakey.replace('_', '');
              newKey = newKey.split('');
              newKey[index] = newKey[index].toUpperCase();
              newKey = newKey.join('');
              odds[newKey] = odds[whatakey];
              delete odds[whatakey];
            }
          );
          return res.send({
            ...event,
            odds,
          });
        });
      });
    } catch (err) {
      console.log(err);
      res.status(500).send('Internal Server Error');
      return;
    }
  }

  updateEventById(req, res) {
    try {
      let eventId = req.params.id;
      console.log(eventId);
      betsService
        .getBetByTwoFields(
          { fieldName: 'event_id', fieldValue: eventId },
          { fieldName: 'win', fieldValue: null }
        )
        .then((bets) => {
          let [w1, w2] = req.body.score.split(':');
          let result;
          if (+w1 > +w2) {
            result = 'w1';
          } else if (+w2 > +w1) {
            result = 'w2';
          } else {
            result = 'x';
          }

          eventsService.updateEventById(eventId, req.body).then(([event]) => {
            Promise.all(
              bets.map((bet) => {
                if (bet.prediction == result) {
                  betsService.updateBetById(bet.id, {
                    win: true,
                  });
                  usersService.getUserById(bet.user_id).then(([user]) => {
                    return usersService.updateUserById(bet.user_id, {
                      balance: user.balance + bet.bet_amount * bet.multiplier,
                    });
                  });
                } else if (bet.prediction != result) {
                  return betsService.updateBetById(bet.id, {
                    win: false,
                  });
                }
              })
            );
            setTimeout(() => {
              [
                'bet_amount',
                'event_id',
                'away_team',
                'home_team',
                'odds_id',
                'start_at',
                'updated_at',
                'created_at',
              ].forEach((whatakey) => {
                let index = whatakey.indexOf('_');
                let newKey = whatakey.replace('_', '');
                newKey = newKey.split('');
                newKey[index] = newKey[index].toUpperCase();
                newKey = newKey.join('');
                event[newKey] = event[whatakey];
                delete event[whatakey];
              });
              res.send(event);
            }, 1000);
          });
        });
    } catch (err) {
      console.log(err);
      res.status(500).send('Internal Server Error');
      return;
    }
  }
}

module.exports = new EventsController();
