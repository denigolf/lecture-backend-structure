const usersService = require('../services/users');
const jwt = require('jsonwebtoken');
const statEmitter = require('../subscribers/emitter');

class UsersController {
  getUserById(req, res) {
    try {
      usersService.getUserById(req.params.id).then(([result]) => {
        if (!result) {
          res.status(404).send({ error: 'User not found' });
          return;
        }
        return res.send({
          ...result,
        });
      });
    } catch (err) {
      console.log(err);
      res.status(500).send('Internal Server Error');
      return;
    }
  }

  createUser(req, res) {
    req.body.balance = 0;
    usersService
      .createUser(req.body)
      .then(([result]) => {
        result.createdAt = result.created_at;
        delete result.created_at;
        result.updatedAt = result.updated_at;
        delete result.updated_at;
        statEmitter.emit('newUser');
        return res.send({
          ...result,
          accessToken: jwt.sign(
            { id: result.id, type: result.type },
            process.env.JWT_SECRET
          ),
        });
      })
      .catch((err) => {
        if (err.code == '23505') {
          res.status(400).send({
            error: err.detail,
          });
          return;
        }
        res.status(500).send('Internal Server Error');
        return;
      });
  }

  updateUserById(req, res) {
    usersService
      .updateUserById(req.params.id, req.body)
      .then(([result]) => {
        return res.send({
          ...result,
        });
      })
      .catch((err) => {
        if (err.code == '23505') {
          console.log(err);
          res.status(400).send({
            error: err.detail,
          });
          return;
        }
        console.log(err);
        res.status(500).send('Internal Server Error');
        return;
      });
  }
}

module.exports = new UsersController();
