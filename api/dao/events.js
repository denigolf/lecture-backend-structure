const db = require('../../db/db');

class EventsDAO {
  getEventById(id) {
    return db('event').where('id', id);
  }

  createEvent(reqBody, oddsId) {
    return db('event')
      .insert({
        ...reqBody,
        odds_id: oddsId,
      })
      .returning('*');
  }

  updateEventById(id, reqBody) {
    return db('event')
      .where('id', id)
      .update({ score: reqBody.score })
      .returning('*');
  }
}

module.exports = new EventsDAO();
