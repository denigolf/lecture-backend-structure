const db = require('../../db/db');

class TransactionsDAO {
  getTransactionById(id) {
    return db('transaction').where('id', id).returning('*');
  }

  createTransaction(reqBody) {
    return db('transaction').insert(reqBody).returning('*');
  }

  updateTransactionById(id, reqBody) {
    return db('transaction').where('id', id).update(reqBody).returning('*');
  }
}

module.exports = new TransactionsDAO();
