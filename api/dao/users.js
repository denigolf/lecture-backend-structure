const db = require('../../db/db');

class UsersDAO {
  select() {
    return db.select().table('user');
  }

  getUserById(id) {
    return db('user').where('id', id).returning('*');
  }

  createUser(reqBody) {
    return db('user').insert(reqBody).returning('*');
  }

  updateUserById(id, reqBody) {
    return db('user').where('id', id).update(reqBody).returning('*');
  }
}

module.exports = new UsersDAO();
