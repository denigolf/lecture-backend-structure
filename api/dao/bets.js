const db = require('../../db/db');

class BetsDAO {
  getBetById(id) {
    return db('bet').where('id', id).returning('*');
  }

  getBetByTwoFields(firstField, secondField) {
    return db('bet')
      .where(firstField.fieldName, firstField.fieldValue)
      .andWhere(secondField.fieldName, secondField.fieldValue);
  }

  createBet(reqBody) {
    return db('bet').insert(reqBody).returning('*');
  }

  updateBetById(id, reqBody) {
    return db('bet').where('id', id).update(reqBody).returning('*');
  }
}

module.exports = new BetsDAO();
