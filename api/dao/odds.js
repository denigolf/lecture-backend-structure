const db = require('../../db/db');

class OddsDAO {
  getOddById(id) {
    return db('odds').where('id', id).returning('*');
  }

  createOdd(reqBody) {
    return db('odds').insert(reqBody).returning('*');
  }

  updateOddById(id, reqBody) {
    return db('odds').where('id', id).update(reqBody).returning('*');
  }
}

module.exports = new OddsDAO();
