const usersDAO = require('../dao/users');

class UsersService {
  select() {
    usersDAO.select();
  }

  getUserById(id) {
    return usersDAO.getUserById(id);
  }

  createUser(reqBody) {
    return usersDAO.createUser(reqBody);
  }

  updateUserById(id, reqBody) {
    return usersDAO.updateUserById(id, reqBody);
  }
}

module.exports = new UsersService();
