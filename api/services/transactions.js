const transactionsDAO = require('../dao/transactions');

class TransactionsService {
  getTransactionById(id) {
    return transactionsDAO.getTransactionById(id);
  }

  createTransaction(reqBody) {
    return transactionsDAO.createTransaction(reqBody);
  }

  updateTransactionById(id, reqBody) {
    return transactionsDAO.updateTransactionById(id, reqBody);
  }
}

module.exports = new TransactionsService();
