const eventsDAO = require('../dao/events');

class EventsService {
  getEventById(id) {
    return eventsDAO.getEventById(id);
  }

  createEvent(reqBody, oddsId) {
    return eventsDAO.createEvent(reqBody, oddsId);
  }

  updateEventById(id, reqBody) {
    return eventsDAO.updateEventById(id, reqBody);
  }
}

module.exports = new EventsService();
