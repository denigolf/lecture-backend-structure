const oddsDAO = require('../dao/odds');

class OddsService {
  getOddById(id) {
    return oddsDAO.getOddById(id);
  }

  createOdd(reqBody) {
    return oddsDAO.createOdd(reqBody);
  }

  updateOddById(id, reqBody) {
    return oddsDAO.updateOddById(id, reqBody);
  }
}

module.exports = new OddsService();
