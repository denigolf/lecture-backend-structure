const betsDAO = require('../dao/bets');

class BetsService {
  getBetById(id) {
    return betsDAO.getBetById(id);
  }

  getBetByTwoFields(firstField, secondField) {
    return betsDAO.getBetByTwoFields(firstField, secondField);
  }

  createBet(reqBody) {
    return betsDAO.createBet(reqBody);
  }

  updateBetById(id, reqBody) {
    return betsDAO.updateBetById(id, reqBody);
  }
}

module.exports = new BetsService();
