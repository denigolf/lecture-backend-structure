const joi = require('joi');
const jwt = require('jsonwebtoken');

const eventsCreateValidation = (req, res, next) => {
  const schema = joi
    .object({
      id: joi.string().uuid(),
      type: joi.string().required(),
      homeTeam: joi.string().required(),
      awayTeam: joi.string().required(),
      startAt: joi.date().required(),
      odds: joi
        .object({
          homeWin: joi.number().min(1.01).required(),
          awayWin: joi.number().min(1.01).required(),
          draw: joi.number().min(1.01).required(),
        })
        .required(),
    })
    .required();
  const isValidResult = schema.validate(req.body);
  if (isValidResult.error) {
    res.status(400).send({ error: isValidResult.error.details[0].message });
    return;
  }

  let token = req.headers['authorization'];
  if (!token) {
    return res.status(401).send({ error: 'Not Authorized' });
  }
  token = token.replace('Bearer ', '');
  try {
    var tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
    if (tokenPayload.type != 'admin') {
      throw new Error();
    }
    next();
  } catch (err) {
    return res.status(401).send({ error: 'Not Authorized' });
  }
};

const eventsUpdateValidation = (req, res, next) => {
  const schema = joi
    .object({
      score: joi.string().required(),
    })
    .required();
  const isValidResult = schema.validate(req.body);
  if (isValidResult.error) {
    res.status(400).send({ error: isValidResult.error.details[0].message });
    return;
  }

  let token = req.headers['authorization'];
  if (!token) {
    return res.status(401).send({ error: 'Not Authorized' });
  }
  token = token.replace('Bearer ', '');
  try {
    var tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
    if (tokenPayload.type != 'admin') {
      throw new Error();
    }
    next();
  } catch (err) {
    console.log(err);
    return res.status(401).send({ error: 'Not Authorized' });
  }
};

module.exports = { eventsCreateValidation, eventsUpdateValidation };
