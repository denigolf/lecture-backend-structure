const joi = require('joi');
const jwt = require('jsonwebtoken');

const transactionsValidation = (req, res, next) => {
  const schema = joi
    .object({
      id: joi.string().uuid(),
      userId: joi.string().uuid().required(),
      cardNumber: joi.string().required(),
      amount: joi.number().min(0).required(),
    })
    .required();
  const isValidResult = schema.validate(req.body);
  if (isValidResult.error) {
    res.status(400).send({ error: isValidResult.error.details[0].message });
    return;
  }

  let token = req.headers['authorization'];
  if (!token) {
    return res.status(401).send({ error: 'Not Authorized' });
  }
  token = token.replace('Bearer ', '');
  try {
    var tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
    if (tokenPayload.type != 'admin') {
      throw new Error();
    }
    next();
  } catch (err) {
    return res.status(401).send({ error: 'Not Authorized' });
  }
};

module.exports = { transactionsValidation };
