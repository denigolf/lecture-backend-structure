const joi = require('joi');
const jwt = require('jsonwebtoken');

const usersGetByIdValidation = (req, res, next) => {
  const schema = joi
    .object({
      id: joi.string().uuid(),
    })
    .required();
  const isValidResult = schema.validate(req.params);
  if (isValidResult.error) {
    res.status(400).send({ error: isValidResult.error.details[0].message });
    return;
  }
  next();
};

const usersCreateValidation = (req, res, next) => {
  const schema = joi
    .object({
      id: joi.string().uuid(),
      type: joi.string().required(),
      email: joi.string().email().required(),
      phone: joi
        .string()
        .pattern(/^\+?3?8?(0\d{9})$/)
        .required(),
      name: joi.string().required(),
      city: joi.string(),
    })
    .required();
  const isValidResult = schema.validate(req.body);
  if (isValidResult.error) {
    res.status(400).send({ error: isValidResult.error.details[0].message });
    return;
  }
  next();
};

const usersUpdateValidation = (req, res, next) => {
  let token = req.headers['authorization'];
  let tokenPayload;
  if (!token) {
    return res.status(401).send({ error: 'Not Authorized' });
  }
  token = token.replace('Bearer ', '');
  try {
    tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
  } catch (err) {
    return res.status(401).send({ error: 'Not Authorized' });
  }
  const schema = joi
    .object({
      email: joi.string().email(),
      phone: joi.string().pattern(/^\+?3?8?(0\d{9})$/),
      name: joi.string(),
      city: joi.string(),
    })
    .required();
  const isValidResult = schema.validate(req.body);
  if (isValidResult.error) {
    res.status(400).send({ error: isValidResult.error.details[0].message });
    return;
  }
  if (req.params.id !== tokenPayload.id) {
    return res.status(401).send({ error: 'UserId mismatch' });
  }
  next();
};

module.exports = {
  usersGetByIdValidation,
  usersCreateValidation,
  usersUpdateValidation,
};
