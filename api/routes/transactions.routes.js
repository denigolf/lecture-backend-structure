const express = require('express');
const {
  transactionsValidation,
} = require('../middlewares/transactions.validation.middleware');
const transactionsController = require('../controllers/transactions');

const router = express.Router();

router.post(
  '/',
  transactionsValidation,
  transactionsController.createTransaction
);

module.exports = router;
