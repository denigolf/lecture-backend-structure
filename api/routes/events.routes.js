const express = require('express');
const {
  eventsCreateValidation,
  eventsUpdateValidation,
} = require('../middlewares/events.validation.middleware');
const eventsController = require('../controllers/events');

const router = express.Router();

router.post('/', eventsCreateValidation, eventsController.createEvent);

router.put('/:id', eventsUpdateValidation, eventsController.updateEventById);

module.exports = router;
