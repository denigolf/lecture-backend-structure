const express = require('express');
const {
  usersGetByIdValidation,
  usersCreateValidation,
  usersUpdateValidation,
} = require('../middlewares/users.validation.middleware');
const usersController = require('../controllers/users');

const router = express.Router();

router.get('/:id', usersGetByIdValidation, usersController.getUserById);

router.post('/', usersCreateValidation, usersController.createUser);

router.put('/:id', usersUpdateValidation, usersController.updateUserById);

module.exports = router;
