const express = require('express');
const { betsValidation } = require('../middlewares/bets.validation.middleware');
const betsController = require('../controllers/bets');

const router = express.Router();

router.post('/', betsValidation, betsController.createBet);

module.exports = router;
