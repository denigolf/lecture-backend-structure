const express = require('express');
const {
  statsValidation,
} = require('../middlewares/stats.validation.middleware');
const statsController = require('../controllers/stats');

const router = express.Router();

router.get('/', statsValidation, statsController.getStats);

module.exports = router;
