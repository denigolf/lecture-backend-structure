const usersRoutes = require('./users.routes');
const healthRoutes = require('./health.routes');
const eventsRoutes = require('./events.routes');
const betsRoutes = require('./bets.routes');
const statsRoutes = require('./stats.routes');
const transactionsRoutes = require('./transactions.routes');

module.exports = (app) => {
  app.use('/users', usersRoutes);
  app.use('/health', healthRoutes);
  app.use('/events', eventsRoutes);
  app.use('/bets', betsRoutes);
  app.use('/stats', statsRoutes);
  app.use('/transactions', transactionsRoutes);
};
